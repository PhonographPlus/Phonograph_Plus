/*
 *  Copyright (c) 2022~2025 chr_56
 */

package player.phonograph.coil.palette

fun interface PaletteContainer {
    fun updatePaletteColor(color: Int)
}